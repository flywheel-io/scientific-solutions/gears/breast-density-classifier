from breast_density_classifier_operator import ClassifierOperator
from flywheel_operator import SaveResultOperator
import glob
import json
import logging
from typing import Dict, Text
import os
from flywheel_gear_toolkit import GearToolkitContext
import pathlib
import sys
import torch
from monai.deploy.core import Application, env
from monai.deploy.operators.dicom_data_loader_operator import DICOMDataLoaderOperator
from monai.deploy.operators.dicom_series_selector_operator import (
    DICOMSeriesSelectorOperator,
)
from monai.deploy.operators.dicom_series_to_volume_operator import (
    DICOMSeriesToVolumeOperator,
)
from monai.deploy.operators.dicom_text_sr_writer_operator import (
    DICOMTextSRWriterOperator,
    EquipmentInfo,
    ModelInfo,
)
from unzip_operator import UnzipOperator

log = logging.getLogger(__name__)


@env(pip_packages=["monai[all]", "highdicom>=0.18.2", "flywheel-gear-toolkit==0.6.8"])
class BreastClassificationApp(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        model_info = ModelInfo(
            "MONAI Model for Breast Density",
            "BreastDensity",
            "Center for Augmented Intelligence in Imaging, Mayo Clinic, Florida",
        )
        my_equipment = EquipmentInfo(
            manufacturer="MONAI Deploy App SDK", manufacturer_model="DICOM SR Writer"
        )
        my_special_tags = {"SeriesDescription": "Not for clinical use"}

        # select files with following extensions:
        # .dcm, .DCM, .Dcm, .zip, .Zip or .ZIP
        # assumes only one file is present

        input_path = self.context.input_path
        log.info(input_path)
        try:
            file_path = glob.glob(f"{input_path}/*.[DdZz]*")[0]
            log.info(f"Folder provided as input. Processing {file_path}")

            # get the file extension
            file_extension = pathlib.Path(file_path).suffix
            log.info(f"the file type is == {file_extension}")

            study_loader_op = DICOMDataLoaderOperator()
            series_selector_op = DICOMSeriesSelectorOperator(rules=Sample_Rules_Text)
            series_to_vol_op = DICOMSeriesToVolumeOperator()
            classifier_op = ClassifierOperator()
            save_result_op = SaveResultOperator()

            # add Unzip operator for certain file extension types:
            if file_extension in [".zip", ".ZIP", ".Zip"]:
                log.info("zip file found")
                file_unzip_op = UnzipOperator()
                self.add_flow(file_unzip_op, study_loader_op)

            self.add_flow(
                study_loader_op,
                series_selector_op,
                {"dicom_study_list": "dicom_study_list"},
            )
            self.add_flow(
                series_selector_op,
                series_to_vol_op,
                {"study_selected_series_list": "study_selected_series_list"},
            )
            self.add_flow(series_to_vol_op, classifier_op, {"image": "image"})
            self.add_flow(classifier_op, save_result_op)
        except:
            log.error(f"unsupported file type")


# This is a sample series selection rule in JSON, simply selecting a MG series.
# If the study has more than 1 MG series, then all of them will be selected.
# Please see more detail in DICOMSeriesSelectorOperator.
# For list of string values, e.g. "ImageType": ["PRIMARY", "ORIGINAL"], it is a match if all elements
# are all in the multi-value attribute of the DICOM series.

Sample_Rules_Text = """
{
    "selections": [
        {
            "name": "MG Series",
            "conditions": {
                "Modality": "(?i)MG"
            }
        }
    ]
}
"""

if __name__ == "__main__":
    if os.path.exists("/flywheel/v0/"):
        with GearToolkitContext(gear_path="/flywheel/v0/") as fw_context:
            fw_context.init_logging()
    else:
        log.setLevel(logging.DEBUG)
    app = BreastClassificationApp(do_run=True)
