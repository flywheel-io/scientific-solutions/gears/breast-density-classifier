# Implementation Details

This document summarizes all steps to create this gear.

## Pre-conditions

The user has trained the model for the classification task.
In this case, the model was trained by the team at the
Center for Augmented Intelligence in Imaging, Mayo Clinic Florida.
The model weights can be downloaded from [Monai Model Zoo](https://monai.io/model-zoo.html).

## Build MONAI Application Package (MAP) locally

1. All code is stored in a folder called "breast-density-classifier".
2. The following command is run in parent directory of this folder
   to build the MAP locally

monai-deploy package breast-density-classifier \
--base nvcr.io/nvidia/pytorch:23.08-py3 \
--model breast-density-classifier/traced_ts_model.pt \
--tag flywheel/breast-density-classifier:0.1.9
