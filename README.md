# **Breast Density classifier**

## Overview

* This GEAR wraps a pytorch model for classifying breast density on mammogram images *

### Summary

* This gear wraps a model for classifying breast density on mammogram images. 
The model classifies images into 4 classes (A-D). These classes correspond to: 
  1. A: Almost entirely fatty
  2. B: Scattered areas of fibroglandular density
  3. C: Hetrogenous dense
  4. D: Dense

* These classes correspond to the [BI-RADS categories](https://www.cancer.gov/types/breast/breast-changes/dense-breasts#what-are-dense-breasts) defined by the American College of Radiology.
* The model was trained at the Center for Augmented Intelligence in Imaging, Mayo Clinic Florida and  was downloaded from [MONAI Model zoo](https://monai.io/model-zoo.html).
  

### Cite

*Gupta, Vikash, et al. A multi-reconstruction study of breast density estimation using Deep Learning, 2022*
*arXiv preprint: https://arxiv.org/abs/2202.08238*

License: *Apache License, Version 2.0*

### Classification

*Category:* *Utility Gear*

*Gear Level:*

- [ ] Project
- [ ] Subject
- [x] Session
- [x] Acquisition
- [ ] Analysis

----

[[_TOC_]]

----

### Inputs

- *Input image*
  - __Name__: *input-file*
  - __Type__: *file*
  - __Optional__: *false*
  - __Classification__: *file*
  - __Description__: * Can only be a Dicom file*
  - __Notes__: *{Gear accepts one Dicom file}*

### Config

- *{Config-Option}*
  - __Name__: *debug*
  - __Type__: *boolean*
  - __Description__: *Enable debug mode*
  - __Default__: *false*

### Outputs

#### Files

*{Output-File}*

- - __Name__: *output.json*
  - __Type__: *file*
  - __Optional__: *false*
  - __Classification__: *file*
  - __Description__: *This file contains the classification label for the input image*

## Usage

### Description

* The input can be single mammogram image in Dicom format. 
* The output is saved to the Analysis section in the Flywheel platform.
* The meta-data of the file (mammogram image) is updated with the classification result.

#### File Specifications

This Gear has only 1 required input file. The details are below:

##### *{Input-File}*

The input can be single mammogram image in Dicom format.

### Workflow

```mermaid
graph LR;
    A[Input-File]:::input --> C;
    C[Upload] --> D[Parent Container <br> Project, Subject, etc];
    D:::container --> E((Gear));
    E:::gear --> F[Analysis]:::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Upload file to container
2. Select file as input to gear
3. The MONAI operator [DICOMDataLoaderOperator](https://docs.monai.io/projects/monai-deploy-app-sdk/en/latest/modules/_autosummary/monai.deploy.operators.DICOMDataLoaderOperator.html) is used to load the DICOM file.
4. Next the MONAI operator [DICOMSeriesSelectorOperator](https://docs.monai.io/projects/monai-deploy-app-sdk/en/latest/modules/_autosummary/monai.deploy.operators.DICOMSeriesSelectorOperator.html) is used to select DICOM series based on the rules provided.

5. The data is then passed to an Inference Operator which loads and runs the model and then generates a classification output. 
6. If processing is run in flywheel environment, the meta-data of the image file is updated with the classification label. This is handled by a custom operator.
7. Gear places output in Analysis. All output files can be downloaded from Analysis.
8. The output files are available under "Outputs" section in job log.

### Use Cases

#### Use Case 1

__*Conditions__*:

[x] For each timepoint, a single x-ray image is available for analysis. Only DICOM format is currently supported.

**Steps**

1. Upload the zipped nifti files to your Flywheel instance
    a. Follow [these steps](https://docs.flywheel.io/hc/en-us/articles/360008162434) to upload <u>de-identified data to Flywheel </u>

2. Select the subjects' sessions that are to be analyzed
   
    a. ![Session Selection](/images/select_subject.png "Session Selection")

3. Navigate to "Run Gear", select "Utility Gear", and select Breast Density Classifier. 
    a. ![Run Gear](/images/run_gear.png "Run Gear")

4. Select the DICOM file to be classified.

5. Run gear and review the results under Information or in the job log output section. 

### Logging

* Logging will indicate the output for each Operator.
* The processing time will be displayed in the log. 

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]

<!-- markdownlint-disable-file -->
