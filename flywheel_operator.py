import os
from flywheel_gear_toolkit import GearToolkitContext
from typing import Text
import monai.deploy.core as md  # 'md' stands for MONAI Deploy (or can use 'core' instead)
from monai.deploy.core import (
    Application,
    DataPath,
    ExecutionContext,
    Image,
    InputContext,
    IOType,
    Operator,
    OutputContext,
)


@md.input("classification_output", Text, IOType.IN_MEMORY)
@md.output("output", DataPath, IOType.DISK)
@md.env(pip_packages=["flywheel-gear-toolkit==0.6.8"])
class SaveResultOperator(Operator):
    def compute(
        self,
        op_input: InputContext,
        op_output: OutputContext,
        context: ExecutionContext,
    ):
        import json

        classification_output = op_input.get()

        if os.path.exists("/flywheel/v0/"):
            with GearToolkitContext(gear_path="/flywheel/v0/") as fw_context:
                input_file = fw_context.get_input("input-file")
                input_file_dict = fw_context.get_input_file_object("input-file")

                # get the current classification for the file.
                # if it does not exist create a key called Custom.
                # otherwise append the model output to the classification.
                # (before append check that the model output is not present).

                classification = input_file_dict["classification"]

                if "Custom" not in classification:
                    classification["Custom"] = [classification_output]
                elif classification_output not in classification["Custom"]:
                    classification["Custom"].append(classification_output)

                fw_context.metadata.update_file(
                    input_file, classification=classification
                )

                # we check for the job configuration flag "save-json"
                # if save-json is true, then we extract job details
                # and save them to a unique json file

                config_dict = fw_context.config_from_args()
                config_args = config_dict["config"]

                save_json = config_args["save-json"]
                # log.info(f"the save-json option == {save_json}")
                print("save_json===")
                print(save_json)

                if save_json == True:
                    fw_context.metadata.pull_job_info()
                    job_info = fw_context.metadata.job_info
                    gear_name = fw_context.metadata.name
                    json_data = job_info[gear_name]
                    json_data["prediction"] = classification_output

                    gear_version = job_info[gear_name]["job_info"]["version"]
                    gear_job_id = job_info[gear_name]["job_info"]["job_id"]

                    output_path = f"/flywheel/v0/output/{gear_name}-{gear_version}-{gear_job_id}.json"
                    with open(output_path, "w") as fp:
                        json.dump(json_data, fp)

        # when running container outside flywheel context we save the
        # classification output in a json file (output.json)
        else:
            # Get output (folder) path and create the folder if not exists
            output_folder = op_output.get().path
            output_folder.mkdir(parents=True, exist_ok=True)

            # Write result to "output.json"
            output_path = output_folder / "output.json"
            with open(output_path, "w") as fp:
                json.dump(classification_output, fp)
